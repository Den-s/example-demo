<?php

namespace App\Services;

use App\Mail\Task\ChangeStatusMail;
use App\Mail\Task\EmployeeTaskCanceledMail;
use App\Mail\Task\EmployeeTaskCompletedMail;
use App\Mail\Task\EmployeeTaskCreatedMail;
use App\Mail\Task\FreelancerAddedMail;
use App\Mail\Task\FreelancerTaskCanceledMail;
use App\Mail\Task\FreelancerTaskCompletedMail;
use App\Models\Tariff;
use App\Models\Task;
use App\Models\User;
use App\Services\Admin\CategoryAttributesService;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class TasksService
{
    protected $companyEmployeeService;
    protected $sendMailService;
    protected $filterService;
    protected $taskAttributeService;
    protected $categoryAttributesService;

    public function __construct(
        CompanyEmployeeService $companyEmployeeService,
        FilterService $filterService,
        SendMailService $sendMailService,
        TaskAttributeService $taskAttributeService,
        CategoryAttributesService $categoryAttributesService
    ) {
        $this->companyEmployeeService = $companyEmployeeService;
        $this->filterService = $filterService;
        $this->sendMailService = $sendMailService;
        $this->taskAttributeService = $taskAttributeService;
        $this->categoryAttributesService = $categoryAttributesService;
    }

    public function create(User $user, array $dataTasks): array
    {
        $companyBalanceService = new CompanyBalanceService($user->company);
        $result = [];

        $lock = $companyBalanceService->lock(10);

        try {
            $lock->block(10);

            DB::beginTransaction();

            $tasksCollection = collect($dataTasks);

            $tariff = $user->company->tariff;

            // в целях безопасности пересчитываем тариф
            $priceTariff = $this->calculationPriceTariff($tasksCollection->sum('price'), $tariff);

            $balanceAvailable = $companyBalanceService->balanceAvailable($priceTariff);

            if (!$balanceAvailable) {
                DB::rollBack();
                $lock->release();

                throw new Exception(
                    trans('errors.notEnoughMoneyToCreateTask'),
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            foreach ($dataTasks as $dataTask) {
                $result[] = $this->createTask($user, $dataTask);
            }

            DB::commit();

            $lock->release();

            return $result;
        } catch (LockTimeoutException $e) {
            throw new Exception(trans('errors.blockingErrorForTaskCreation') . ': ' . $e->getMessage());
        } finally {
            optional($lock)->release();
        }
    }

    public function createOne(User $user, array $data): Task
    {
        $result = $this->create($user, [$data]);

        return $result[0];
    }

    protected function createTask(User $user, array $data): Task
    {
        $additionalData = [
            'company_id' => $user->company->id,
            'creator_id' => $user->id,
            'tariff_id' => $user->company->tariff->id,
            'deadline' => !empty($data['deadline']) ? date('Y-m-d', strtotime($data['deadline'])) : null,
            'status' => Task::STATUS['AWAITING_CONFIRMATION'],
        ];

        $data['price_tariff'] = $this->calculationPriceTariff($data['price'], $user->company->tariff);

        $data['report_need'] = $data['report_need'] == 'false' ? 0 : 1;
        $attributes = $data['attributes'];

        unset($data['deadline']);
        unset($data['files']);
        unset($data['attributes']);
        $data = array_merge($additionalData, $data);

        $task = $user->company->tasks()->create($data);

        //Создание атрибутов к задаче.
        //Update потому что создание атрибутов происходит в админке при создании в списке атрибутов
        $this->taskAttributeService->update($task, $attributes);

        return $task;
    }

    public function update(array $tasks, User $user, array $data): array
    {
        $companyBalanceService = new CompanyBalanceService($user->company);
        $result = [];

        $lock = $companyBalanceService->lock(10);

        try {
            $lock->block(10);

            DB::beginTransaction();

            /** @var Task $task */
            foreach ($tasks as $task) {
                $companyPay = $task->companyPay;

                $task->fill([
                    'status' => $data['status'],
                    'date_completed' => now(),
                ]);

                // Если статус задачи переходит в завершённый, а так же, если нет платежа по этой задаче
                // в таблице company_pay, то создаём запись.
                // Этим самым мы проводим платёж по задаче в сторону фрилансера
                if (in_array($data['status'], Task::getStatusesCompleted()) && !$companyPay) {
                    $task->companyPay()->create([
                        'company_id' => $task->company_id,
                        'user_id' => $user->id,
                        'task_id' => $task->id,
                        'executor_id' => $task->executor_id,
                        'amount' => $task->price,
                        'amount_tariff' => $task->price_tariff,
                    ]);
                }

                $task->save();

                $result[] = $task;
            }

            DB::commit();

            $lock->release();

            return $result;
        } catch (LockTimeoutException $e) {
            throw new Exception(trans('errors.blockingErrorForChangingTaskStatus') . ': ' . $e->getMessage());
        } finally {
            optional($lock)->release();
        }
    }

    public function updateOne(Task $task, User $user, array $data)
    {
        $tasks = $this->update([$task], $user, $data);

        return $tasks[0];
    }

    public function updateEntire(Task $task, array $data): void
    {
        $task->fill([
            'name' => $data['name'],
            'description' => $data['description'],
            'category_id' => $data['category_id'],
        ]);

        $task->save();

        $attributes = $this->taskAttributeService->prepareFromEdit($data['attributes']);

        $this->taskAttributeService->update($task, $attributes);
    }

    public function freelancerUpdateStatus(Task $task, array $data): void
    {
        $task->fill([
            'status' => $data['status'],
        ]);

        $task->save();
    }

    public function getByCompany(int $userId, Request $request)
    {
        $companyUser = $this->companyEmployeeService->getCompanyUser($userId);

        if (!empty($companyUser->id)) {
            $query = Task::with(['executor', 'creator', 'category'])
                ->where('company_id', $companyUser->company_id);

            return $this->filterService->applyFilter($query, $request);
        }

        return null;
    }

    public function getByUser(Request $request, int $userId)
    {
        $query = Task::with(['creator', 'executor', 'category', 'company'])
            ->where('executor_id', $userId);

        return $this->filterService->applyFilter($query, $request);
    }

    public function getAll(Request $request)
    {
        $query = Task::with(['creator', 'executor', 'category', 'company']);

        return $this->filterService->applyFilter($query, $request);
    }

    public function getOne(int $id, string $userType)
    {
        $task = Task::with(['executor', 'creator', 'category', 'tariff', 'company'])
            ->where('id', $id)
            ->first();

        $categoryRelations = $this->categoryAttributesService->getRelation($task->category_id);

        foreach ($categoryRelations as $relation) {
            $slug = CategoryAttributesService::ATTRIBUTE_PREFIX . $relation->categoryAttribute->slug;

            //особенность пакета, ленивая загрузка
            $task->$slug;
        }

        $statuses = [];
        $statusUser = [];

        // для администратора отдаём статусы с общими названиями - statuses_all
        // для остальных пользователей названия статусов преобразуются
        if ($userType != Task::USER_TYPE['ADMIN']) {
            $statuses = Task::STATUS_TREE[$task->status][$userType] ?? [];
            $statusUser = Task::STATUS_USER[app()->getLocale()][$userType];
        }

        $mediaEmployee = $task->getMedia(Task::MEDIA_COLLECTION['EMPLOYEE']);
        $mediaFreelancer = $task->getMedia(Task::MEDIA_COLLECTION['FREELANCER']);

        return [
            'task' => $task,
            'statuses' => $statuses,
            'statuses_all' => $this->getAllStatuses('ru'),
            'status_user' => $statusUser,
            'media_employee' => $mediaEmployee,
            'media_freelancer' => $mediaFreelancer,
        ];
    }

    public function getTariff(int $userId)
    {
        $companyUser = $this->companyEmployeeService->getCompanyUser($userId);

        if (!empty($companyUser->id)) {
            return $companyUser->company->tariff;
        }

        return null;
    }

    public function getAllStatuses(string $lang): array
    {
        switch ($lang) {
            case 'ru':
                return Task::STATUS_RUS;
                break;
        }
    }

    public function getUserStatuses(string $userType): array
    {
        return Task::STATUS_USER[app()->getLocale()][$userType];
    }

    public function addFiles(Task $task, array $files, string $mediaCollection): void
    {
        foreach ($files as $key => $file) {
            $task->addMedia($file)->toMediaCollection($mediaCollection, 's3');
        }
    }

    public function downloadOneMedia(array $data): array
    {
        $task = Task::find($data['model_id']);
        $mediaItems = $task->getMedia($data['media_collection']);
        $temporaryS3Url = ''; //$mediaItems[0]->getTemporaryUrl(Carbon::now()->addMinutes(5));
        $fileName = '';

        foreach ($mediaItems as $item) {
            if ($item->id == $data['media_id']) {
                $temporaryS3Url = $item->getTemporaryUrl(Carbon::now()->addMinutes(5));
                $fileName = $item->file_name;
            }
        }

        return [
            'url' => $temporaryS3Url,
            'file_name' => $fileName,
        ];
    }

    public function deleteOneMedia(array $data): void
    {
        $task = Task::find($data['model_id']);
        $mediaItems = $task->getMedia($data['media_collection']);

        foreach ($mediaItems as $item) {
            if ($item->id == $data['media_id']) {
                $item->delete();
            }
        }
    }

    public function addMedia(int $taskId, array $files, string $mediaCollection): void
    {
        $task = Task::find($taskId);

        $this->addFiles($task, $files, $mediaCollection);
    }

    /**
     * Тариф к задаче высчитываем на бэке
     * фронту не доверяем
     *
     * @param float $price
     * @param Tariff $tariff
     * @return float
     */
    public function calculationPriceTariff(float $price, Tariff $tariff): float
    {
        $priceTariff =  $price * (100 + $tariff->value) / 100;
        $priceTariff = round($priceTariff, 2);

        return $priceTariff;
    }

    public function getFreelancerCompleted(Request $request)
    {
        $query = Task::with(['creator', 'executor', 'category', 'company'])
            ->where('executor_id', $request->input('executor_id'))
            ->whereIn('status', Task::getStatusesCompleted());

        return $this->filterService->applyFilter($query, $request);
    }

    public function emailTaskCreated(Task $task): void
    {
        // отправка письма фрилансеру
        $this->sendMailService->send(
            FreelancerAddedMail::class,
            $task->executor->email,
            ['task_id' => $task->id]
        );

        // отправка письма заказчику
        $this->sendMailService->send(
            EmployeeTaskCreatedMail::class,
            $task->creator->email,
            ['task_id' => $task->id]
        );
    }

    public function emailChangeStatus(Task $task): void
    {
        // отправка письма фрилансеру
        $this->sendMailService->send(
            ChangeStatusMail::class,
            $task->executor->email,
            ['task' => $task]
        );

        // отправка письма заказчику
        $this->sendMailService->send(
            ChangeStatusMail::class,
            $task->creator->email,
            ['task' => $task]
        );
    }

    public function emailStatusCompleted(Task $task): void
    {
        // отправка письма фрилансеру
        $this->sendMailService->send(
            FreelancerTaskCompletedMail::class,
            $task->executor->email,
            ['task' => $task]
        );

        // отправка письма заказчику
        $this->sendMailService->send(
            EmployeeTaskCompletedMail::class,
            $task->creator->email,
            ['task' => $task]
        );
    }

    public function emailStatusCanceled(Task $task): void
    {
        // отправка письма фрилансеру
        $this->sendMailService->send(
            FreelancerTaskCanceledMail::class,
            $task->executor->email,
            ['task' => $task]
        );

        // отправка письма заказчику
        $this->sendMailService->send(
            EmployeeTaskCanceledMail::class,
            $task->creator->email,
            ['task' => $task]
        );
    }
}
