<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string|null $surname
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $phone
 * @property int $phone_verified
 * @property string $type
 * @property int $reg_code
 * @property string $password
 * @property string|null $remember_token
 * @property int|null $restore_code
 * @property string|null $last_login
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \App\Models\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanyPay[] $companyPayToExecutor
 * @property-read int|null $company_pay_to_executor_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FreelancerPayOut[] $freelancerPayOut
 * @property-read int|null $freelancer_pay_out_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Access[] $userAccess
 * @property-read int|null $user_access_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User employees()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User freelancers()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhoneVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRegCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRestoreCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanyFreelancer[] $freelancerCompanies
 * @property-read int|null $freelancer_companies_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User admins()
 * @property int $sms_code SMS код при входе (авторизации)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSmsCode($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OauthAccessToken[] $OauthTwoAcessToken
 * @property-read int|null $oauth_two_acess_token_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $freelancerTasks
 * @property-read int|null $freelancer_tasks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FreelancerPayList[] $freelancerPayList
 * @property-read int|null $freelancer_pay_list_count
 */
class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, Notifiable, HasMediaTrait;

    const TYPE = [
        'ADMIN' => 'admin',
        'FREELANCER' => 'freelancer',
        'EMPLOYEE' => 'employee',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'email_verified_at', 'password', 'phone', 'phone_verified', 'type',
        'reg_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getPublicUsers(): array
    {
        return [
            self::TYPE['FREELANCER'],
            self::TYPE['EMPLOYEE'],
        ];
    }

    public static function getProtectedUsers(): array
    {
        return [
            self::TYPE['ADMIN'],
        ];
    }

    public function currentAccess(): array
    {
        $userPrivileges = self::with('userAccess')
            ->where('id', $this->id)
            ->first();

        $privileges = [];
        foreach ($userPrivileges->userAccess as $value) {
            $privileges[] = $value->name_access;
        }

        return $privileges;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEmployees($query)
    {
        return $query->where('type', self::TYPE['EMPLOYEE']);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFreelancers($query)
    {
        return $query->where('type', self::TYPE['FREELANCER']);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdmins($query)
    {
        return $query->where('type', self::TYPE['ADMIN']);
    }

    public function userAccess()
    {
        return $this->hasManyThrough(
            'App\Models\Access',
            'App\Models\UserAccess',
            'user_id',
            'id',
            'id',
            'access_id'
        );
    }

    public function company()
    {
        return $this->hasOneThrough(
            'App\Models\Company',
            'App\Models\CompanyUser',
            'user_id',
            'id',
            'id',
            'company_id'
        );
    }

    public function freelancerCompanies()
    {
        return $this->hasManyThrough(
            'App\Models\Company',
            'App\Models\CompanyFreelancer',
            'user_id',
            'id',
            'id',
            'company_id'
        );
    }

    public function companyPayToExecutor()
    {
        return $this->hasMany(CompanyPay::class, 'executor_id', 'id');
    }

    public function freelancerPayOut()
    {
        return $this->hasMany(FreelancerPayOut::class, 'user_id', 'id');
    }

    public function freelancerTasks()
    {
        return $this->hasMany(Task::class, 'executor_id', 'id');
    }

    public function freelancerPayList()
    {
        return $this->hasMany(FreelancerPayList::class, 'user_id', 'id');
    }
}
