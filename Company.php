<?php

namespace App\Models;

use App\Enums\CompanyMediaCollections;
use App\Enums\CompanyTaxSystems;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * App\Models\Company
 *
 * @property int $id
 * @property int $tariff_id
 * @property string $country
 * @property string $company_name
 * @property string $company_name_full
 * @property string|null $project_name
 * @property string|null $site
 * @property string $inn
 * @property string|null $okpo
 * @property string $bank
 * @property string $ogrn
 * @property string $kpp
 * @property string $bik
 * @property string $payment_account
 * @property string $correspondent_account
 * @property string $legal_address
 * @property string $mailing_address
 * @property string|null $director
 * @property string $status
 * @property int $contract_number Номера договора, каждый новый месяц отсчёт начинается с еденицы
 * @property string|null $accepted_at Дата успешной верификации компании
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanyFreelancer[] $companyFreelancer
 * @property-read int|null $company_freelancer_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanyPay[] $companyPays
 * @property-read int|null $company_pays_count
 * @property-read \App\Models\CompanyUser $companyUser
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invoice[] $invoices
 * @property-read int|null $invoices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \App\Models\CompanyUser $owner
 * @property-read \App\Models\Tariff $tariff
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereAcceptedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereBik($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCompanyNameFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereContractNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCorrespondentAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereDirector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereKpp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereLegalAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereMailingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereOgrn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereOkpo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company wherePaymentAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereProjectName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereTariffId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $contract
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InvoiceSimple[] $invoiceSimple
 * @property-read int|null $invoice_simple_count
 * @property-read mixed $media_agreement
 */
class Company extends Model implements Auditable, HasMedia
{
    use \OwenIt\Auditing\Auditable,
        HasMediaTrait;

    const STATUS = [
        'NEW' => 'new',
        'ON_VERIFICATION' => 'on_verification',
        'ACCEPTED' => 'accepted',
        'DENIED' => 'denied',
    ];

    const CONTRACT_START_NUMBER = 1;

    protected $table = 'companies';

    protected $guarded = [];

    /**
     * Список дополнительных свойств
    */
    protected $appends = ['contract', 'media_agreement'];

    /**
     * Добавляем дополнительное поле/свойство - сгенерированный номер договора
    */
    public function getContractAttribute()
    {
        $result = '';

        if ($this->accepted_at) {
            $result = date('Y/m', strtotime($this->accepted_at)) . '/' .
                $this->contract_number . '-' .
                $this->id;
        }

        return $this->attributes['contract'] = $result;
    }

    /**
     * Добавляем дополнительное поле/свойство - медиа данные о договоре
     * в случае отсутствия данных возвращает пустой массив
     */
    public function getMediaAgreementAttribute()
    {
        return $this->attributes['media_agreement'] = $this->getMedia(CompanyMediaCollections::AGREEMENT()->value);
    }

    public static function getTaxSystems(): array
    {
        return [
            CompanyTaxSystems::OSNO()->value => trans('text.company.osno'),
            CompanyTaxSystems::USN()->value => trans('text.company.usn'),
            CompanyTaxSystems::OTHER()->value => trans('text.company.other'),
        ];
    }

    public function tariff()
    {
        return $this->hasOne(Tariff::class, 'id', 'tariff_id');
    }

    /**
     * Для выбора владельца компании, обязательно должно быть условие owner = 1
    */
    public function companyUser()
    {
        return $this->hasOne(CompanyUser::class, 'company_id', 'id');
    }

    public function companyUsers()
    {
        return $this->hasMany(CompanyUser::class, 'company_id', 'id');
    }

    public function owner()
    {
        return $this->hasOne(CompanyUser::class, 'company_id', 'id')
            ->where('owner', true);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'company_id', 'id');
    }

    public function invoiceSimple()
    {
        return $this->hasMany(InvoiceSimple::class, 'company_id', 'id');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class, 'company_id', 'id');
    }

    public function companyPays()
    {
        return $this->hasMany(CompanyPay::class, 'company_id', 'id');
    }

    public function companyFreelancer()
    {
        return $this->hasMany(CompanyFreelancer::class, 'company_id', 'id');
    }
}
