<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetOneRequest;
use App\Http\Requests\Tasks\CreateRequest;
use App\Http\Requests\Tasks\GetOneMediaRequest;
use App\Http\Requests\Tasks\GetUserStatusesRequest;
use App\Http\Requests\Tasks\UpdateEntireRequest;
use App\Http\Requests\Tasks\UpdateRequest;
use App\Models\Task;
use App\Services\CompanyBalanceService;
use App\Services\TaskAttributeService;
use App\Services\TasksService;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
    protected $tasksService;
    protected $taskAttributeService;

    public function __construct(TasksService $tasksService, TaskAttributeService $taskAttributeService)
    {
        $this->tasksService = $tasksService;
        $this->taskAttributeService = $taskAttributeService;
    }

    public function create(CreateRequest $request)
    {
        try {
            $task = $this->tasksService->createOne(Auth::user(), $request->all());
            $this->tasksService->addFiles($task, $request->file('files', []), Task::MEDIA_COLLECTION['EMPLOYEE']);
            $this->tasksService->emailTaskCreated($task);
        } catch (\Exception $exception) {
            throw new HttpResponseException(response()->json([
                'message' => $exception->getMessage()
            ], Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Частичное обновление, действует в просмотре задачи
     * меняется только статус
     *
     * @param Task $task
     * @param UpdateRequest $request
     */
    public function update(Task $task, UpdateRequest $request)
    {
        $task = $this->tasksService->updateOne($task, Auth::user(), $request->all());

        // отправка писем в зависимости от статуса
        switch ($task->status) {
            case in_array($task->status, Task::getStatusesCanceled()):
                $this->tasksService->emailStatusCanceled($task);
                break;
            case in_array($task->status, Task::getStatusesCompleted()):
                $this->tasksService->emailStatusCompleted($task);
                break;
            default:
                $this->tasksService->emailChangeStatus($task);
        }
    }

    /**
     * Полное обновление. Из редактирования задачи
     * На текущий момент обновляются не все поля, в будущем может быть расширен
     *
     * @param Task $task
     * @param UpdateEntireRequest $request
     */
    public function updateEntire(Task $task, UpdateEntireRequest $request)
    {
        $this->tasksService->updateEntire($task, $request->all());
    }

    public function getByCompany(Request $request)
    {
        return $this->tasksService->getByCompany(Auth::id(), $request);
    }

    public function getOne(GetOneRequest $request)
    {
        return $this->tasksService->getOne($request->input('id'), Task::USER_TYPE['EMPLOYEE']);
    }

    public function getTariff()
    {
        return $this->tasksService->getTariff(Auth::id());
    }

    public function downloadOneMedia(GetOneMediaRequest $request)
    {
        return $this->tasksService->downloadOneMedia($request->all());
    }

    public function deleteOneMedia(GetOneMediaRequest $request)
    {
        $this->tasksService->deleteOneMedia($request->all());
    }

    public function addMedia(GetOneRequest $request)
    {
        $this->tasksService->addMedia(
            $request->input('id'),
            $request->file('files', []),
            Task::MEDIA_COLLECTION['EMPLOYEE']
        );
    }

    public function getAllStatuses(): array
    {
        return $this->tasksService->getAllStatuses('ru');
    }

    public function getUserStatuses(GetUserStatusesRequest $request): array
    {
        return $this->tasksService->getUserStatuses($request->input('user_type'));
    }
}
