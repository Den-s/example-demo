<?php

namespace App\Http\Requests\Tasks;

use App\Models\Company;
use App\Models\User;
use App\Services\CompanyBalanceService;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = $this->user();

        return $user->phone && $user->phone_verified == 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'category_id' => ['required', 'integer', 'exists:categories,id'],
            'executor_id' => ['required', 'integer', 'exists:users,id'],
            'price' => ['required', 'numeric', 'min:100'],
            'price_tariff' => ['required', 'numeric', 'min:100'],
            'attributes' => ['required', 'array'],
            'deadline' => ['required', 'sometimes', 'after:yesterday'],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        /** @var User $user */
        $user = $this->user();

        $data = parent::all();
        $companyBalanceService = new CompanyBalanceService($user->company);
        $balance = $companyBalanceService->getBalance();

        $companyVerify = $user->company && $user->company->status == Company::STATUS['ACCEPTED'];
        $companyFreelancers = $user->company && $user->company->companyFreelancer->count();

        $freelancer = $user->company->companyFreelancer->where('user_id', $data['executor_id'])->all();

        $validator->after(function ($validator) use ($balance, $companyVerify, $companyFreelancers, $freelancer) {
            if ($balance['free_money'] < 1) {
                $validator->errors()->add('balance', trans('errors.replenishCompanyAccount'));
            }

            if (!$companyVerify) {
                $validator->errors()->add('companyVerify', trans('errors.addCompanyDetails'));
            }

            if (!$companyFreelancers) {
                $validator->errors()->add('companyFreelancers', trans('errors.addFreelancers'));
            }

            if (!$freelancer) {
                $validator->errors()->add('companyFreelancers', trans('errors.freelancerNotFound'));
            }
        });
    }
}
